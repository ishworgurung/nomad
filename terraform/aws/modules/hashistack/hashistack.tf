variable "name" {}
variable "region" {}
variable "ami" {}
variable "instance_type" {}
variable "key_name" {}
variable "server_count" {}
variable "client_count" {}
variable "retry_join" {}
variable "nomad_binary" {}
variable "client_count_spot" {}

data "aws_vpc" "default" {
  default = true
}

variable "internal_cidr" {
  type = "list"

  default = [
    "10.0.0.0/8",
    "172.16.0.0/12",
    "192.168.0.0/16",
  ]
}

resource "aws_security_group" "primary" {
  name   = "${var.name}"
  vpc_id = "${data.aws_vpc.default.id}"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = "${var.internal_cidr}"
  }

  # Nomad
  ingress {
    from_port   = 4646
    to_port     = 4646
    protocol    = "tcp"
    cidr_blocks = "${var.internal_cidr}"
  }

  # Consul
  ingress {
    from_port   = 8500
    to_port     = 8500
    protocol    = "tcp"
    cidr_blocks = "${var.internal_cidr}"
  }

  # HDFS NameNode UI
  ingress {
    from_port = 50070
    to_port   = 50070
    protocol  = "tcp"

    cidr_blocks = "${var.internal_cidr}"
  }

  # HDFS DataNode UI
  ingress {
    from_port   = 50075
    to_port     = 50075
    protocol    = "tcp"
    cidr_blocks = "${var.internal_cidr}"
  }

  # Spark history server UI
  ingress {
    from_port   = 18080
    to_port     = 18080
    protocol    = "tcp"
    cidr_blocks = "${var.internal_cidr}"
  }

  ingress {
    from_port = 0
    to_port   = 0
    protocol  = "-1"
    self      = true
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

data "template_file" "user_data_server" {
  template = "${file("${path.root}/user-data-server.sh")}"

  vars {
    server_count = "${var.server_count}"
    region       = "${var.region}"
    retry_join   = "${var.retry_join}"
    nomad_binary = "${var.nomad_binary}"
  }
}

data "template_file" "user_data_client" {
  template = "${file("${path.root}/user-data-client.sh")}"

  vars {
    region       = "${var.region}"
    retry_join   = "${var.retry_join}"
    nomad_binary = "${var.nomad_binary}"
  }
}

resource "aws_instance" "server" {
  ami                    = "${var.ami}"
  instance_type          = "${var.instance_type}"
  key_name               = "${var.key_name}"
  vpc_security_group_ids = ["${aws_security_group.primary.id}"]
  count                  = "${var.server_count}"

  #Instance tags
  tags {
    Name           = "${var.name}-server-${count.index}"
    ConsulAutoJoin = "auto-join"
  }

  user_data            = "${data.template_file.user_data_server.rendered}"
  iam_instance_profile = "${aws_iam_instance_profile.instance_profile.name}"
}

resource "aws_instance" "client" {
  ami                    = "${var.ami}"
  instance_type          = "${var.instance_type}"
  key_name               = "${var.key_name}"
  vpc_security_group_ids = ["${aws_security_group.primary.id}"]
  count                  = "${var.client_count}"
  depends_on             = ["aws_instance.server"]

  #Instance tags
  tags {
    Name           = "${var.name}-client-${count.index}"
    ConsulAutoJoin = "auto-join"
  }

  ebs_block_device = {
    device_name           = "/dev/xvdd"
    volume_type           = "gp2"
    volume_size           = "50"
    delete_on_termination = "true"
  }

  user_data            = "${data.template_file.user_data_client.rendered}"
  iam_instance_profile = "${aws_iam_instance_profile.instance_profile.name}"
}

resource "aws_spot_instance_request" "client" {
  ami                             = "${var.ami}"
  instance_type                   = "${var.instance_type}"
  key_name                        = "${var.key_name}"
  vpc_security_group_ids          = ["${aws_security_group.primary.id}"]
  count                           = "${var.client_count_spot}"
  depends_on                      = ["aws_instance.server"]
  spot_price                      = "1.03"
  wait_for_fulfillment            = true
  instance_interruption_behaviour = "terminate"

  #Instance tags
  tags {
    Name           = "${var.name}-cspot-${count.index}"
    ConsulAutoJoin = "auto-join"
  }

  ebs_block_device = {
    device_name           = "/dev/xvdd"
    volume_type           = "gp2"
    volume_size           = "50"
    delete_on_termination = "true"
  }

  user_data            = "${data.template_file.user_data_client.rendered}"
  iam_instance_profile = "${aws_iam_instance_profile.instance_profile.name}"
}

resource "aws_iam_instance_profile" "instance_profile" {
  name_prefix = "${var.name}"
  role        = "${aws_iam_role.instance_role.name}"
}

resource "aws_iam_role" "instance_role" {
  name_prefix        = "${var.name}"
  assume_role_policy = "${data.aws_iam_policy_document.instance_role.json}"
}

data "aws_iam_policy_document" "instance_role" {
  statement {
    effect  = "Allow"
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
  }
}

resource "aws_iam_role_policy" "auto_discover_cluster" {
  name   = "auto-discover-cluster"
  role   = "${aws_iam_role.instance_role.id}"
  policy = "${data.aws_iam_policy_document.auto_discover_cluster.json}"
}

data "aws_iam_policy_document" "auto_discover_cluster" {
  statement {
    effect = "Allow"

    // actions = [
    //   "ec2:DescribeInstances",
    //   "ec2:DescribeTags",
    //   "autoscaling:DescribeAutoScalingGroups",
    // ]
    actions = ["*"] //FIXME

    resources = ["*"]
  }
}

output "server_public_ips" {
  value = ["${aws_instance.server.*.public_ip}"]
}

output "client_public_ips" {
  value = ["${aws_instance.client.*.public_ip}"]
}

output "client_spot_public_ips" {
  value = ["${aws_spot_instance_request.client.*.public_ip}"]
}
